FROM continuumio/miniconda3:latest

COPY librerias.txt /

WORKDIR /notebooks

EXPOSE 8888

RUN pip install --upgrade pip && \
     pip install --user --no-cache-dir -r /librerias.txt && \
     pip install --no-cache-dir notebook && pip install jupyterlab
