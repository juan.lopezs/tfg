# TFG

## Dockerfile
Genera la imagen docker necesaria para la ejecución del contenedor. Se compila con el comando:
*sudo docker build -t <nombre> .*

## librerias.txt
Contiene las librerías python necesarias (y sus versiones correspondientes) para la ejecución del código.

## run_docker.sh
Script que ejecuta el contendedor. Se debe ejecutar como *sudo*, y monta 2 volúmenes:
    1. la subcarpeta /notebooks
    2. la carpeta local de Dropbox, donde se encuentran los datos* 
Además, usa el puerto *8888* para habilitar los *jupyter notebooks*.

* En este caso, deberiamos ejecutar el comando como usuario local, pero no es necesario porque los datos ya están leidos


## /notebooks/run_jupyter.sh
Una vez dentro del contenedor, este script ejecuta *jupyter notebook* de forma que se puede acceder a esto desde la URL localhost:8888<br>
Por simplicidad, se omite el uso de tokens de sesión.
