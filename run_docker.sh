#!/bin/bash

if test $# -ne 1
then
    echo "Error: debes introducir 1 argumento: el nombre de la imagen del contenedor"
    exit
fi

docker_name=$1
# Para montar la carpeta de Dropbox habria que ejecutarlo como usuario normal
# (no es necesario porque los datos estan guardados en 'notebooks/valid_data.pkl')
sudo docker run -it -v ${PWD}/notebooks:/notebooks \
    -v /home/$USER/Dropbox/TFG-2022-Juan\ Lopez\ Salar/datos_etiquetados:/notebooks/data \
    -p 8888:8888 \
    $docker_name




